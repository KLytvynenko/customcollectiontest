using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;

namespace UITest.Droid.Views
{
    [Activity(Label = "View for FirstViewModel")]
    public class TestView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.FirstView);
        }
    }
}