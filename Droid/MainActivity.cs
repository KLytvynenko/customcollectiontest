﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace TestMulti.Droid
{
    [Activity(Label = "TestMulti.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            // This will load all tests within the current project
            var nunit = new NUnit.Runner.App();

            // If you want to add tests in another assembly
            nunit.AddTestAssembly(typeof(Core.Tests.CollectionTest).Assembly);

            // Do you want to automatically run tests when the app starts?
            nunit.Options = new NUnit.Runner.Services.TestOptions
            {
                AutoRun = true,
                TerminateAfterExecution = false,
                CreateXmlResultFile = false

            };

            LoadApplication(nunit);
        }
    }
}
