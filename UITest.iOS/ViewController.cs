﻿using System;
using Cirrious.FluentLayouts.Touch;
using Core;
using Core.Tests;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.ExtensionMethods;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.iOS.Views;
using UIKit;
using UITest.iOS.Cells;

namespace UITest.iOS
{
    [Register("ViewController")]
    public class ViewController : MvxViewController<TestViewModel>
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            var table = new UITableView() { BackgroundColor = UIColor.Red, TranslatesAutoresizingMaskIntoConstraints = false };
            var source = new MvxSimpleTableViewSource(table, typeof(TableCell), TableCell.Key);
            table.Source = source;
            var set = this.CreateBindingSet<ViewController, TestViewModel>();
            set.Bind(source).To(vm => vm.Source.Source);
            set.Apply();

            View.Add(table);
            View.AddConstraints(
                table.AtLeftOf(View),
                table.AtRightOf(View),
                table.AtBottomOf(View),
                table.AtTopOf(View)
            );
        }
    }
}
