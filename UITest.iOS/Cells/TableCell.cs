﻿using System;
using Core.Models;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;

namespace UITest.iOS.Cells
{
    public class TableCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("TableCell");
        public TableCell()
        {
            Initialize();
        }

        public TableCell(IntPtr handle) : base(handle)
        {
            Initialize();
        }

        void Initialize()
        {
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<TableCell, Message>();
                set.Bind(TextLabel).To(vm => vm.ServerReceiveTime);
                set.Apply();
            });
        }
    }
}
