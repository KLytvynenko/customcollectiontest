﻿using System;
using System.Collections.Generic;

namespace Core
{
    public class MessageComparer : IComparer<Message>
    {
        private MessageComparer() { }
        public static IComparer<Message> Instance => _instance;
        private static readonly IComparer<Message> _instance = new MessageComparer();

        public int Compare(Message x, Message y)
        {
            return y.ServerReceiveTime.CompareTo(x.ServerReceiveTime);
        }
    }
}
