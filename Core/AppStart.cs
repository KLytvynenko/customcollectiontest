﻿using System;
using MvvmCross.Core.ViewModels;

namespace Core
{
    public class AppStart : MvxNavigatingObject, IMvxAppStart
    {
        public void Start(object hint = null)
        {
            ShowViewModel<TestViewModel>();
        }
    }
}
