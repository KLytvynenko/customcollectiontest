﻿using System;
using System.Threading.Tasks;
using Core.Collections;
using Core.Comparer;
using Core.Models;
using Core.Tests;
using MvvmCross.Core.ViewModels;

namespace Core
{
    public class TestViewModel : MvxViewModel
    {
        MessageCollection _source;
        public MessageCollection Source
        {
            get { return _source; }
            set { SetProperty(ref _source, value); }
        }

        public async void Init()
        {
            await StartTests();
        }

        public async Task StartTests()
        {
            base.Start();
            var test = new CollectionTest();
            test.Collection = new MessageCollection(MessageComparer.Instance);
            Source = test.Collection;
            test.EmptyCollectionClear();
            Source.Clear();
            await RunTest(test.MessageCollectionTest());
            await RunTest(test.MessageCollectionTest1());
            await RunTest(test.MessageDelete());
            await RunTest(test.MessageAddDeletedMessage());
            await RunTest(test.MessageDeleteMoreThanExists());
            await RunTest(test.MessageDeleteBetweenAddsMessage());
            await RunTest(test.MessageDeleteAndManipulateMessage());
            await RunTest(test.DeleteMessages());
        }

        async Task RunTest(Task test)
        {
            try
            {
                await Task.Delay(500);
                await test;
            }
            finally {
                Source.Clear();
            }
        }
    }
}
