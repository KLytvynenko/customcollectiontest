﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Core.Models;
using MvvmCross.Core.ViewModels;

namespace Core.Collections
{
    public class MessageCollection : CustomCollection<Message>
    {
        public MessageCollection(IComparer<Message> comparable) : base(comparable)
        {
        }

        protected override void HandleItems(IEnumerable<Message> list)
        {
            System.Diagnostics.Debug.WriteLine("start hadle items");
            foreach (var message in list)
                HandleItem(this[message.Id], message);
        }

        protected override void RemoveItems(IEnumerable<Message> list)
        {
                foreach (var item in list)
                {
                    var foundItem = this[item.Id];
                    if (foundItem != null)
                    {
                        var index = _collection.IndexOf(foundItem);
                        AddChanges(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, foundItem, index));
                        _collection.RemoveAt(index);
                    }
                }
        }

        Message this[string id] => _collection?.FirstOrDefault(x => x.Id == id);


        bool IfHasZeroPosition(Message msg)
        {
            return _collection.Count == 0 || msg.CreateTimeUnix > _collection.ElementAt(0).CreateTimeUnix;
        }

        void HandleItem(Message oldMessage, Message newMessage)

        {
            if (oldMessage == null)
                InsertNewItem(newMessage);
            else //oldMessage.NeedUpdate(message))
                UpdateItem(oldMessage, newMessage);
        }

        void InsertNewItem(Message newMessage)
        {
            int newIndex = 0;
            if (!IfHasZeroPosition(newMessage))
                newIndex = ~_collection.BinarySearch(newMessage, _comparare);

            InsertItem(newIndex, newMessage);
        }

        void UpdateItem(Message foundItem, Message message)

        {
            int oldIndex = _collection.IndexOf(foundItem);
            bool isPositionChanged = foundItem.ServerReceiveTime == message.ServerReceiveTime || oldIndex == 0;
            foundItem.Update(message);
            if (message.Status == MessageStatus.IsRemoved)
                AddChanges(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, foundItem, foundItem, oldIndex));
            else if (!isPositionChanged)
                MoveItem(oldIndex, foundItem);
        }

        void MoveItem(int oldIndex, Message newMessage)
        {
            _collection.RemoveAt(oldIndex);
            var newIndex = ~_collection.BinarySearch(newMessage, _comparare);
            _collection.Insert(newIndex, newMessage);
            System.Diagnostics.Debug.WriteLine($"move item from{oldIndex} to {newIndex}");
            AddChanges(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, newMessage, newIndex, oldIndex));
        }
    }
}
