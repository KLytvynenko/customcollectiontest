﻿using System;
using System.Collections.Specialized;
using MvvmCross.Core.ViewModels;

namespace Core.Collections
{
    public class BaseObservableCollection<T> : MvxObservableCollection<T>
    {
        public BaseObservableCollection()
        {
        }

        public BaseObservableCollection(System.Collections.Generic.IEnumerable<T> items) : base(items)
        {
        }

        public void CollectionChange(NotifyCollectionChangedEventArgs args)
        {
            OnCollectionChanged(args);
        }

        public void RefreshItem(int index, T item)
        {
            base.SetItem(index, item);
        }
    }
}
