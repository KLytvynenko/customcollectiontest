﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;

namespace Core.Collections
{
    public abstract class CustomCollection<IItem> : MvxNotifyPropertyChanged where IItem : class
    {
        ReaderWriterLockSlim _rwLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        readonly IList<NotifyCollectionChangedEventArgs> _notification = new List<NotifyCollectionChangedEventArgs>();
        volatile protected BaseObservableCollection<IItem> _collection;
        protected IComparer<IItem> _comparare;

        protected abstract void HandleItems(IEnumerable<IItem> list);
        protected abstract void RemoveItems(IEnumerable<IItem> list);

        public CustomCollection(IComparer<IItem> comparable)
        {
            _comparare = comparable;
        }

        public IEnumerable<IItem> Source
        {
            get
            {
                _rwLock.EnterReadLock();
                try
                {
                    return _collection;
                }
                finally
                {
                    _rwLock.ExitReadLock();
                }
            }
        }

        public IEnumerable<IItem> GetItems(Func<IItem, bool> predicate)
        {
            _rwLock.EnterReadLock();
            try
            {
                return _collection?.Where(predicate);
            }
            finally
            {
                _rwLock.ExitReadLock();
            }
        }

        public void HandleRange(IEnumerable<IItem> list)
        {
            try
            {
                _rwLock.EnterWriteLock();
                if (_collection == null)
                {
                    System.Diagnostics.Debug.WriteLine($"InitCollection count {list?.Count()}");
                    InitCollection(list);
                    RaisePropertyChanged(nameof(Source));
                }
                else
                {
                    SetSuppressEvent(true);
                    HandleItems(list);
                    SetSuppressEvent(false);
                }
            }
            finally
            {
                _rwLock.ExitWriteLock();
                NotifyCollectionChange();
            }
        }

        public void RemoveRange(IEnumerable<IItem> list)
        {
            if (_collection == null || _collection.Count == 0)
                return;

            _rwLock.EnterWriteLock();
            try
            {
                SetSuppressEvent(true);
                RemoveItems(list);
                SetSuppressEvent(false);
            }
            finally
            {
                _rwLock.ExitWriteLock();
                NotifyCollectionChange();
            }
        }

        public void Remove(IItem item)
        {
            if (_collection == null || _collection.Count == 0)
                return;

            _rwLock.EnterWriteLock();
            try
            {
                SetSuppressEvent(true);
                var index = _collection.IndexOf(item);
                if (index == -1)
                    return;
                _collection?.Remove(item);
                AddChanges(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
                SetSuppressEvent(false);
            }
            finally
            {
                _rwLock.ExitWriteLock();
                NotifyCollectionChange();
            }
        }

        public int Count()
        {
            _rwLock.EnterReadLock();
            try
            {
                return _collection?.Count ?? 0;
            }
            finally
            {
                _rwLock.ExitReadLock();
            }
        }

        public void Clear()
        {
            if (_collection == null)
                return;

            _rwLock.EnterWriteLock();
            try
            {
                _collection.Clear();
                _collection = null;
            }
            finally
            {
                _rwLock.ExitWriteLock();
            }
        }

        public IItem this[int index]
        {
            get
            {
                _rwLock.EnterReadLock();
                try
                {
                    return _collection?.ElementAt(index);
                }
                finally
                {
                    _rwLock.ExitReadLock();
                }
            }
        }

        protected virtual void InitCollection(IEnumerable<IItem> list)
        {
            var newList = list.ToList();
            newList.Sort(_comparare);
            _collection = new BaseObservableCollection<IItem>(newList);
        }

        protected void AddChanges(NotifyCollectionChangedEventArgs args)
        {
            _notification.Add(args);
        }

        void NotifyCollectionChange()
        {
            try
            {
                _rwLock.EnterWriteLock();
                if (_notification == null || _notification.Count == 0)
                    return;

                for (int i = 0; i < _notification.Count; i++)
                    _collection.CollectionChange(_notification[i]);

                _notification.Clear();
            }
            finally
            {
                _rwLock.ExitWriteLock();
            }
        }

        void SetSuppressEvent(bool enable)
        {
            System.Diagnostics.Debug.WriteLine($"SetSuppressEvent {enable}");
            _collection.SuppressEvents = enable;
        }

        protected void InsertItem(int newIndex, IItem certainObj)
        {
            _collection.Insert(newIndex, certainObj);
            System.Diagnostics.Debug.WriteLine($"insert item {newIndex}");
            AddChanges(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, certainObj, newIndex));
        }

        //public void RefreshItem(int index, IItem message)
        //{
        //    _collection.RefreshItem(index, message);
        //}
    }
}
