﻿using System;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;

namespace Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            //CreatableTypes()
            //    .EndingWith("Service")
            //    .AsInterfaces()
            //    .RegisterAsLazySingleton();

            Mvx.ConstructAndRegisterSingleton<IMvxAppStart, AppStart>();
            CreateDefaultViewModelLocator();
            RegisterAppStart(Mvx.Resolve<IMvxAppStart>());
        }

        protected override IMvxViewModelLocator CreateDefaultViewModelLocator()
        {
            // register the instance
            if (!Mvx.CanResolve<IMvxViewModelLocator>())
            {
                var locator = base.CreateDefaultViewModelLocator();
                Mvx.RegisterSingleton<IMvxViewModelLocator>(locator);
            }

            return Mvx.Resolve<IMvxViewModelLocator>();
        }
    }
}
