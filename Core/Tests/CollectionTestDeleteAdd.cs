﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Collections;
using Core.Comparer;
using NUnit.Framework;

namespace Core.Tests
{
    public partial class CollectionTest
    {
        [Test]
        public async Task MessageDeleteAndManipulateMessage()
        {
            await Task.WhenAll(
                Task.Run(() => RemoveItems(0, 100)),
                Task.Run(() => InsertItems(200, 100)),
                Task.Run(() => RemoveItems(200, 50)),
                Task.Run(() => InsertItems(300, 100))).ConfigureAwait(false);
            Assert.IsTrue(CheckIsSorted());
        }

        [Test]
        public async Task MessageAddDeletedMessage()
        {
            await Task.WhenAll(
                Task.Run(() => InsertItems(200, 100)),
                Task.Run(() => RemoveItems(200, 70)),
                Task.Run(() => InsertItems(300, 100)),
                Task.Run(() => RemoveItems(200, 100))).ConfigureAwait(false);
            Assert.IsTrue(CheckIsSorted());
        }

        [Test]
        public async Task MessageDeleteBetweenAddsMessage()
        {
            var numberToAdd = 100;
            var numberToDelete = 50;

            InsertItems(0, numberToAdd);
            RemoveItems(0, numberToDelete);
            InsertItems(numberToDelete, numberToAdd);

            Assert.IsTrue(CheckIsSorted());
            Assert.AreEqual(100, Collection.Count());
        }
    }
}
