﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Collections;
using Core.Comparer;
using NUnit.Framework;

namespace Core.Tests
{
    public partial class CollectionTest
    {
        [Test]
        public void MessageNullDelete()
        {
            Collection.Remove(null);
            Assert.IsNull(Collection.Source);
        }

        [Test]
        public async Task MessageDelete()
        {
            InsertItems(0, 10);
            var itemToDelete = Collection.Source.First();
            Collection.Remove(itemToDelete);
            Assert.AreEqual(9, Collection.Count());
            Assert.IsFalse(Collection.Source.Contains(itemToDelete));
        }

        [Test]
        public async Task DeleteMessages()
        {
            var numberToAdd = 100;
            var numberToDelete = 5;

            InsertItems(0, numberToAdd);
            await Task.WhenAll(
                Task.Run(() => RemoveItems(0, numberToDelete)),
                Task.Run(() => RemoveItems(5, numberToDelete)),
                Task.Run(() => RemoveItems(10, numberToDelete))).ConfigureAwait(false);

            Assert.IsTrue(CheckIsSorted());
            Assert.AreEqual(85, Collection.Source.Count());
        }

        [Test]
        public async Task MessageDeleteMoreThanExists()
        {
            var numberToAdd = 100;
            var numberToDelete = 50;

            InsertItems(0, numberToAdd);
            await Task.WhenAll(
                Task.Run(() => RemoveItems(0, numberToDelete)),
                Task.Run(() => RemoveItems(5, numberToDelete)),
                Task.Run(() => RemoveItems(0, numberToDelete))
            ).ConfigureAwait(false);
            Assert.IsTrue(CheckIsSorted());
        }

        [Test]
        public async Task MessageDeleteOnEmptyCollection()
        {
            var numberToDelete = 50;
            await Task.WhenAll(
                Task.Run(() => RemoveItems(0, numberToDelete)),
                Task.Run(() => RemoveItems(50, numberToDelete)),
                Task.Run(() => RemoveItems(100, numberToDelete))).ConfigureAwait(false);
            Assert.IsNull(Collection.Source);
        }

        void RemoveItems(int from, int to)
        {
            var messages = GenerageMessage(from, to);
            Collection.RemoveRange(messages);
        }
    }
}
