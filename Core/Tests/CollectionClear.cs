﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Collections;
using Core.Comparer;
using MvvmCross.Core.ViewModels;
using NUnit.Framework;

namespace Core.Tests
{
    public partial class CollectionTest
    {
        [Test]
        public void EmptyCollectionClear()
        {
            Collection.Clear();
            Assert.AreEqual(0,Collection.Count());
        }

        [Test]
        public async Task ClearCollection()
        {
            await Task.WhenAll(
                Task.Run(() => InsertItems(0, 100)),
                Task.Run(() => RemoveItems(5, 20)),
                Task.Run(() => InsertItems(500, 300))).ConfigureAwait(false);
            Collection.Clear();
            Assert.AreEqual(0, Collection.Count());
        }
    }
}
