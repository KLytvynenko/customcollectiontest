﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Collections;
using Core.Comparer;
using Core.Models;
using MvvmCross.Core.ViewModels;
using NUnit.Framework;

namespace Core.Tests
{
    public partial class CollectionTest : BaseTest
    {
       [Test]
        public async Task MessageCollectionTest()
        {
            await Task.WhenAll(
                Task.Run(() => InsertItems(0, 50)),
                Task.Run(() => InsertItems(0, 70)),
                Task.Run(() => InsertItems(0, 100))).ConfigureAwait(false);
            Assert.IsTrue(CheckIsSorted());
        }

        [Test]
        public async Task MessageCollectionTest1()
        {
            await Task.WhenAll(
               Task.Run(() => InsertItems(0, 10)),
               Task.Run(() => InsertItems(20, 10)),
               Task.Run(() => InsertItems(10, 30))).ConfigureAwait(false);
            Assert.IsTrue(CheckIsSorted());
        }

        bool CheckIsSorted()
        {
            long? serverTime = null;
            foreach (var item in Collection.Source)
            {
                if (serverTime == null || serverTime >= item.ServerReceiveTime)
                {
                    serverTime = item.ServerReceiveTime;
                    continue;
                }
                return false;
            }
            return true;
        }

        IEnumerable<Message> GenerageMessage(int from, int to)
        {
            return Enumerable.Range(from, to).Select(x => new Message()
            {
                CreateTimeUnix = x,
                ServerReceiveTime = DateTime.UtcNow.Ticks,
                Status = (MessageStatus)(x % 4),
                Id = x.ToString()
            });
        }

        void InsertItems(int from, int to)
        {
            var messages = GenerageMessage(from, to);
            Collection.HandleRange(messages);
        }
    }
}
