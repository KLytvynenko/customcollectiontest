﻿using System;
using Core.Collections;
using Core.Comparer;
using NUnit.Framework;

namespace Core.Tests
{
    public class BaseTest
    {
        public MessageCollection Collection { get; set; }

        [OneTimeSetUp]
        public void Init()
        {
            Collection = new MessageCollection(MessageComparer.Instance);
        }

        [TearDown]
        public void AfterTest()
        {
            Collection = new MessageCollection(MessageComparer.Instance);
        }
    }
}
