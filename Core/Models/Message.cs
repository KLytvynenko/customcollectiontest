﻿using System;
namespace Core.Models
{
    public enum MessageStatus
    {
        IsFormed = 0,
        IsSend = 1,
        IsDelivered = 2,
        IsViewed = 3,
        IsRemoved = 4,
        IsFailed = -1
    }

    public class Message
    {
        public string Id { get; set; }
        public int CreateTimeUnix { get; set; }
        public long ServerReceiveTime { get; set; }
        public MessageStatus Status { get; set; }
        public void Update(Message newMessage)
        {
            ServerReceiveTime = newMessage.ServerReceiveTime;
            Status = newMessage.Status;
        }
    }
}
