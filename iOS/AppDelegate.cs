﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace TestMulti.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            var nunit = new NUnit.Runner.App();

            // If you want to add tests in another assembly
            nunit.AddTestAssembly(typeof(Core.Tests.CollectionTest).Assembly);

            // Do you want to automatically run tests when the app starts?
            nunit.Options = new NUnit.Runner.Services.TestOptions
            {
                AutoRun = true,
                TerminateAfterExecution = false,
                CreateXmlResultFile = false

            };

            LoadApplication(nunit);

            return base.FinishedLaunching(app, options);
        }
    }
}
